# skybeard3-compose



## Getting started

Clone the repository, then in it's root folder, clone the dalai repository
```
git clone https://gitlab.com/skybeard/skybeard3-compose.git
cd skybeard3-compose
git clone https://github.com/cocktailpeanut/dalai.git
```

Build the services with 
```
docker compose build
```


Download the 13B llambda model

```
cd dalai
docker compose run dalai npx dalai llambda install 7B
cd ..
 ```

Add your telegram api key to the skybeard-telegram-config.yaml file, then run the services:
```
docker compose up -d
```
Your bot is now ready to go!

